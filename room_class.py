from pathlib import Path

class Player:
    backpack = []
    lives = 5

    def __init__ (self, name):
        self.name = name

    def __str__ (self):
        return(f"My name is {self.name}.")

    def show_backpack (self):
        if len(self.backpack) == 0:
            print("Backpack is empty.")
        else:
            print(f"You have: {self.backpack} in backpack")

    def add_to_backpack (self, item):
        self.item = item
        self.backpack.append(self.item)
        return f"You pick up {self.item} in your backpack"

    def lose_live(self):
        self.lives -= 1
        return print(f"You lost your life, you have left {self.lives} lives.\nThink again about the answer.")

    def check_lives(self):
        if self.lives == 0:
            print("Game over! You have lost all your lives")
            exit()

class Rooms(Player):
    def reader_txt(self, file_name):
        '''Read file txt from CWD'''
        self.file_name = file_name
        path_entry = Path.cwd() / file_name
        try:
            with path_entry.open(mode="r", encoding="utf-8") as file:
                text = file.read()
                print(text)
        except FileNotFoundError:
            print(f"File {file_name} not found.")
            exit()

    def room1_1(self):
        anser1_1 = True
        while anser1_1:
            self.check_lives()
            self.reader_txt("room1_1.txt")
            if input().lower() == "red":
                self.reader_txt("room2_1.txt")
                anser1_1 = False
            else:
                self.lose_live()

    def room2_2(self):
        '''First possition in room choice where you go'''
        self.reader_txt("room2_2.txt")
        anser2_2 = input().lower()
        if anser2_2 == "1" or anser2_2 == "right" or anser2_2 == "go to the cabinet with a drawer on the right":
            self.room2_2_right()
        elif anser2_2 == "2" or anser2_2 == "left" or anser2_2 == "go to the safe on the left":
            self.room2_2_left()
        elif anser2_2 == "3" or anser2_2 == "door" or anser2_2 == "will come to the door with the painting":
            self.room2_2_door()
        elif anser2_2 == "4" or anser2_2 == "backpack":
            self.show_backpack()
            self.room2_2()

    def room2_2_right(self):
        '''Right room with cipher and key'''
        if "key" in self.backpack:
            print("You've already been here, the cipher is empty.")
            self.room2_2()
        else:
            if input("There is a cupboard with a drawer in front of you, do you want to open the drawer? 'yes' or 'no'\n").lower() == "yes":
                if input("There's a key in the drawer. If you want to take it write 'take key', if not write 'back'\n").lower() == "take key":
                    self.add_to_backpack("key")
                    print("You got the key and put it in your backpack.")
                    self.room2_2()
                else:
                    self.room2_2()
            else:
                self.room2_2()

    def room2_2_left(self):
        '''Room with safe to open it need key from room right'''
        if "key" not in self.backpack:
            print("The safe is locked and you need a key. Find the key!")
            self.room2_2()
        elif "key" in self.backpack and "UV lamp" not in self.backpack:
            if input("The safe is locked. 'Use key' and open safe. Or go 'back'.\n").lower() == "use key":
                if input(
                        "There is a UV lamp in the safe. 'Take UV lamp' you may need it in the future.\n").lower() == "take uv lamp":
                    self.add_to_backpack("uv lamp")
                    print("You just added a lamp to your backpack.")
                    self.room2_2()
                else:
                    self.room2_2()
            else:
                self.room2_2()
        else:
            print("You've already been here, the cipher is empty.")
            self.room2_2()

    def room2_2_door(self):
        '''Last room with door and painting to use uv lamp'''
        if "uv lamp" in self.backpack:
            if input(
                    "You see a image with UV written on it. Maybe it's worth shining a light on it 'use UV lamp'?\n").lower() == "use uv lamp":
                print("The number 123 appears in the image under the UV light. Remember him.\n")
                correct_numer = True
                anser_door = input(
                    "You go to the door to enter the code. \nOops, the digital lock has seven positions and only two numbers 1 and 0 can be selected in each position. \nEnter the code:\n")
                while correct_numer:
                    if anser_door == "1111011":
                        print("Congratulations, you've passed the first room. Next one soon...")
                        correct_numer = False
                    else:
                        self.check_lives()
                        self.lose_live()
                        anser_door = input("The door does not open, you entered the wrong code, please try again.\n")
            else:
                self.room2_2()
        else:
            print("Unfortunately, you don't have enough items see code. Find the extra item and go back.")
            self.room2_2()