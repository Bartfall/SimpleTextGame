from pathlib import Path
from room_class import Player

def reader_txt(file_name):
    '''Read file txt from CWD'''
    path_entry = Path.cwd() / file_name
    try:
        with path_entry.open(mode="r", encoding="utf-8") as file:
            text = file.read()
            print(text)
    except FileNotFoundError:
        print(f"File {file_name} not found.")
        exit()
#print prolog
reader_txt("entry.txt")
player = Player(input("What is your name :\n"))

#chosen a door in room1_1
anser1_1 = True
while anser1_1:
    player.check_lives()
    reader_txt("room1_1.txt")
    if input().lower() == "red":
        anser1_1 = False
    else:
        player.lose_live()

#print room2_1
reader_txt("room2_1.txt")
def room2_2():
    '''First possition in room choice where you go'''
    reader_txt("room2_2.txt")
    anser2_2 = input().lower()
    if anser2_2 == "1" or anser2_2 == "right" or anser2_2 == "go to the cabinet with a drawer on the right":
        room2_2_right()
    elif anser2_2 == "2" or anser2_2 == "left" or anser2_2 == "go to the safe on the left":
        room2_2_left()
    elif anser2_2 == "3" or anser2_2 == "door" or anser2_2 == "will come to the door with the painting":
        room2_2_door()
    elif anser2_2 == "4" or anser2_2 == "backpack":
        player.show_backpack()
        room2_2()
def room2_2_right():
    '''Right room with cipher and key'''
    if "key" in player.backpack:
        print("You've already been here, the cipher is empty.")
        room2_2()
    else:
        if input("There is a cupboard with a drawer in front of you, do you want to open the drawer? 'yes' or 'no'\n").lower() == "yes":
            if input("There's a key in the drawer. If you want to take it write 'take key', if not write 'back'\n").lower() == "take key":
                player.add_to_backpack("key")
                print("You got the key and put it in your backpack.")
                room2_2()
            else:
                room2_2()
        else:
            room2_2()

def room2_2_left():
    '''Room with safe to open it need key from room right'''
    if "key" not in player.backpack:
        print("The safe is locked and you need a key. Find the key!")
        room2_2()
    elif "key" in player.backpack and "UV lamp" not in player.backpack:
        if input("The safe is locked. 'Use key' and open safe. Or go 'back'.\n").lower() == "use key":
            if input("There is a UV lamp in the safe. 'Take UV lamp' you may need it in the future.\n").lower() == "take uv lamp":
                player.add_to_backpack("uv lamp")
                print("You just added a lamp to your backpack.")
                room2_2()
            else:
                room2_2()
        else:
            room2_2()
    else:
        print("You've already been here, the cipher is empty.")
        room2_2()
def room2_2_door():
    '''Last room with door and painting to use uv lamp'''
    if "uv lamp" in player.backpack:
        if input("You see a image with UV written on it. Maybe it's worth shining a light on it 'use UV lamp'?\n").lower() == "use uv lamp":
            print("The number 123 appears in the image under the UV light. Remember him.\n")
            correct_numer = True
            anser_door = input("You go to the door to enter the code. \nOops, the digital lock has seven positions and only two numbers 1 and 0 can be selected in each position. \nEnter the code:\n")
            while correct_numer:
                if anser_door == "1111011":
                    print("Congratulations, you've passed the first room. Next one soon...")
                    correct_numer = False
                else:
                    player.check_lives()
                    player.lose_live()
                    anser_door = input("The door does not open, you entered the wrong code, please try again.\n")
        else:
            room2_2()
    else:
        print("Unfortunately, you don't have enough items see code. Find the extra item and go back.")
        room2_2()

room2_2()
